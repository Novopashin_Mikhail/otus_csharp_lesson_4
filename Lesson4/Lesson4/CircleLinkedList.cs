using System.Collections;
using System.Collections.Generic;

namespace Lesson4
{
    public class CircleLinkedList<T> : IEnumerable<T>
    {
        private Node<T> _root;
        private Node<T> _current;
        private int _capacity;
        private bool _isEmpty;
        
        public class Node<T>
        {
            public Node<T> NextNode { get;set; }
            public T Value { get; set; }
        }
        
        public CircleLinkedList(){}
        
        public CircleLinkedList(Node<T> root)
        {
            _root = root;
        }

        public void Add(T value)
        {
            var node = new Node<T>
            {
                Value = value
            };
            if (_root == null)
            {
                _root = node;
                _current = node;
                _current.NextNode = _root;
            }
            else
            {
                node.NextNode = _root;
                _current.NextNode = node;
                _current = node;
            }

            _capacity++;
        }
        
        public bool Remove(T value)
        {
            Node<T> current = _root;
            Node<T> previous = null;
 
            if (_isEmpty) return false;
 
            do
            {
                if (current.Value.Equals(value))
                {
                    if (previous != null)
                    {
                        previous.NextNode = current.NextNode;
                        if (current == _current)
                            _current = previous;
                    }
                    else
                    {
                        if(_capacity==1)
                        {
                            _root = _current = null;
                        }
                        else
                        {
                            _root = current.NextNode;
                            _current.NextNode = current.NextNode;
                        }
                    }
                    _capacity--;
                    return true;
                }
 
                previous = current;
                current = current.NextNode;
            } while (current != _root);
 
            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            Node<T> current = _root;
            do
            {
                if (current != null)
                {
                    yield return current.Value;
                    current = current.NextNode;
                }
            }
            while (true);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}