﻿using System;
using NUnit.Framework;

namespace Lesson4.Test
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {
            var list = new CircleLinkedList<string>();
            list.Add("A");
            list.Add("B");
            list.Add("C");

            var str = string.Empty;
            int i = 0;
            foreach (var item in list)
            {
                Console.WriteLine(item);
                i++;
                str = item;
                if (i == 4)
                    break;
            }
            
            Assert.AreEqual(str, "A");
        }
    }
}